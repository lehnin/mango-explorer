{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "opposite-territory",
   "metadata": {},
   "source": [
    "# ⚠ Warning\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n",
    "\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/OpinionatedGeek%2Fmango-explorer/HEAD?filepath=LiquidationProcessor.ipynb) _🏃‍♀️ To run this notebook press the ⏩ icon in the toolbar above._\n",
    "\n",
    "[🥭 Mango Markets](https://mango.markets/) support is available at: [Docs](https://docs.mango.markets/) | [Discord](https://discord.gg/67jySBhxrg) | [Twitter](https://twitter.com/mangomarkets) | [Github](https://github.com/blockworks-foundation) | [Email](mailto:hello@blockworks.foundation)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "thorough-portland",
   "metadata": {},
   "source": [
    "# 🥭 Liquidation Processor\n",
    "\n",
    "This notebook contains a liquidator that makes heavy use of [RX Observables](https://rxpy.readthedocs.io/en/latest/reference_observable.html).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dependent-metro",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "import rx\n",
    "import rx.operators as ops\n",
    "import time\n",
    "import typing\n",
    "\n",
    "from AccountLiquidator import AccountLiquidator\n",
    "from BaseModel import Group, LiquidationEvent, MarginAccount, MarginAccountMetadata, TokenValue\n",
    "from Context import Context\n",
    "from Observables import EventSource\n",
    "from WalletBalancer import NullWalletBalancer, WalletBalancer\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecological-salem",
   "metadata": {},
   "source": [
    "# 💧 LiquidationProcessor class\n",
    "\n",
    "An `AccountLiquidator` liquidates a `MarginAccount`. A `LiquidationProcessor` processes a list of `MarginAccount`s, determines if they're liquidatable, and calls an `AccountLiquidator` to do the work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "published-murder",
   "metadata": {},
   "outputs": [],
   "source": [
    "class LiquidationProcessor:\n",
    "    def __init__(self, context: Context, account_liquidator: AccountLiquidator, wallet_balancer: WalletBalancer, worthwhile_threshold: float = 0.01):\n",
    "        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)\n",
    "        self.context: Context = context\n",
    "        self.account_liquidator: AccountLiquidator = account_liquidator\n",
    "        self.wallet_balancer: WalletBalancer = wallet_balancer\n",
    "        self.worthwhile_threshold: float = worthwhile_threshold\n",
    "        self.liquidations: EventSource[LiquidationEvent] = EventSource[LiquidationEvent]()\n",
    "        self.ripe_accounts: typing.Optional[typing.List[MarginAccount]] = None\n",
    "\n",
    "    def update_margin_accounts(self, ripe_margin_accounts: typing.List[MarginAccount]):\n",
    "        self.logger.info(f\"Received {len(ripe_margin_accounts)} ripe 🥭 margin accounts to process.\")\n",
    "        self.ripe_accounts = ripe_margin_accounts\n",
    "\n",
    "    def update_prices(self, prices):\n",
    "        started_at = time.time()\n",
    "\n",
    "        if self.ripe_accounts is None:\n",
    "            self.logger.info(\"Ripe accounts is None - skipping\")\n",
    "            return\n",
    "\n",
    "        self.logger.info(f\"Running on {len(self.ripe_accounts)} ripe accounts.\")\n",
    "        group = Group.load(self.context)\n",
    "        updated: typing.List[MarginAccountMetadata] = []\n",
    "        for margin_account in self.ripe_accounts:\n",
    "            balance_sheet = margin_account.get_balance_sheet_totals(group, prices)\n",
    "            balances = margin_account.get_intrinsic_balances(group)\n",
    "            updated += [MarginAccountMetadata(margin_account, balance_sheet, balances)]\n",
    "\n",
    "        liquidatable = list(filter(lambda mam: mam.balance_sheet.collateral_ratio <= group.maint_coll_ratio, updated))\n",
    "        self.logger.info(f\"Of those {len(updated)}, {len(liquidatable)} are liquidatable.\")\n",
    "\n",
    "        above_water = list(filter(lambda mam: mam.collateral_ratio > 1, liquidatable))\n",
    "        self.logger.info(f\"Of those {len(liquidatable)} liquidatable margin accounts, {len(above_water)} are 'above water' margin accounts with assets greater than their liabilities.\")\n",
    "\n",
    "        worthwhile = list(filter(lambda mam: mam.assets - mam.liabilities > self.worthwhile_threshold, above_water))\n",
    "        self.logger.info(f\"Of those {len(above_water)} above water margin accounts, {len(worthwhile)} are worthwhile margin accounts with more than ${self.worthwhile_threshold} net assets.\")\n",
    "\n",
    "        self._liquidate_all(group, prices, worthwhile)\n",
    "\n",
    "        time_taken = time.time() - started_at\n",
    "        self.logger.info(f\"Check of all ripe 🥭 accounts complete. Time taken: {time_taken:.2f} seconds.\")\n",
    "\n",
    "    def _liquidate_all(self, group: Group, prices: typing.List[TokenValue], to_liquidate: typing.List[MarginAccountMetadata]):\n",
    "        to_process = to_liquidate\n",
    "        while len(to_process) > 0:\n",
    "            highest_first = sorted(to_process, key=lambda mam: mam.assets - mam.liabilities, reverse=True)\n",
    "            highest = highest_first[0]\n",
    "            try:\n",
    "                self.account_liquidator.liquidate(group, highest.margin_account, prices)\n",
    "                self.wallet_balancer.balance(prices)\n",
    "\n",
    "                updated_margin_account = MarginAccount.load(self.context, highest.margin_account.address, group)\n",
    "                balance_sheet = updated_margin_account.get_balance_sheet_totals(group, prices)\n",
    "                balances = updated_margin_account.get_intrinsic_balances(group)\n",
    "                updated_mam = MarginAccountMetadata(updated_margin_account, balance_sheet, balances)\n",
    "                if updated_mam.assets - updated_mam.liabilities > self.worthwhile_threshold:\n",
    "                    self.logger.info(f\"Margin account {updated_margin_account.address} has been drained and is no longer worthwhile.\")\n",
    "                else:\n",
    "                    self.logger.info(f\"Margin account {updated_margin_account.address} is still worthwhile - putting it back on list.\")\n",
    "                    to_process += [updated_mam]\n",
    "            except Exception as exception:\n",
    "                self.logger.error(f\"Failed to liquidate account '{highest.margin_account.address}' - {exception}\")\n",
    "            finally:\n",
    "                # highest should always be in to_process, but we're outside the try-except block\n",
    "                # so let's be a little paranoid about it.\n",
    "                if highest in to_process:\n",
    "                    to_process.remove(highest)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "polar-killer",
   "metadata": {},
   "source": [
    "# 🏃 Running\n",
    "\n",
    "A quick example to show how to plug observables into the `LiquidationProcessor`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "rough-sending",
   "metadata": {},
   "outputs": [],
   "source": [
    "if __name__ == \"__main__\":\n",
    "    from AccountLiquidator import NullAccountLiquidator\n",
    "    from Context import default_context\n",
    "    from Observables import create_backpressure_skipping_observer, log_subscription_error\n",
    "    from Wallet import default_wallet\n",
    "\n",
    "    from rx.scheduler import ThreadPoolScheduler\n",
    "\n",
    "    if default_wallet is None:\n",
    "        raise Exception(\"No wallet\")\n",
    "\n",
    "    pool_scheduler = ThreadPoolScheduler(2)\n",
    "\n",
    "    def fetch_prices(context):\n",
    "        group = Group.load(context)\n",
    "\n",
    "        def _fetch_prices(_):\n",
    "            return group.fetch_token_prices()\n",
    "\n",
    "        return _fetch_prices\n",
    "\n",
    "    def fetch_margin_accounts(context):\n",
    "        def _fetch_margin_accounts(_):\n",
    "            group = Group.load(context)\n",
    "            return MarginAccount.load_all_for_group_with_open_orders(context, context.program_id, group)\n",
    "        return _fetch_margin_accounts\n",
    "\n",
    "    liquidation_processor = LiquidationProcessor(default_context, NullAccountLiquidator(), NullWalletBalancer())\n",
    "\n",
    "    print(\"Starting margin account fetcher subscription\")\n",
    "    margin_account_interval = 60\n",
    "    margin_account_subscription = rx.interval(margin_account_interval).pipe(\n",
    "        ops.subscribe_on(pool_scheduler),\n",
    "        ops.start_with(-1),\n",
    "        ops.map(fetch_margin_accounts(default_context)),\n",
    "    ).subscribe(create_backpressure_skipping_observer(on_next=liquidation_processor.update_margin_accounts, on_error=log_subscription_error))\n",
    "\n",
    "    print(\"Starting price fetcher subscription\")\n",
    "    price_interval = 2\n",
    "    price_subscription = rx.interval(price_interval).pipe(\n",
    "        ops.subscribe_on(pool_scheduler),\n",
    "        ops.map(fetch_prices(default_context))\n",
    "    ).subscribe(create_backpressure_skipping_observer(on_next=liquidation_processor.update_prices, on_error=log_subscription_error))\n",
    "\n",
    "    print(\"Subscriptions created - now just running\")\n",
    "\n",
    "    time.sleep(120)\n",
    "    print(\"Disposing\")\n",
    "    price_subscription.dispose()\n",
    "    margin_account_subscription.dispose()\n",
    "    print(\"Disposed\")\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
