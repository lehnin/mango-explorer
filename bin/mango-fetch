#!/usr/bin/env pyston3

import os
import sys

from pathlib import Path

# Get the full path to this script.
script_path = Path(os.path.realpath(__file__))

# The parent of the script is the bin directory.
# The parent of the bin directory is the notebook directory.
# It's this notebook directory we want.
notebook_directory = script_path.parent.parent

# Add the notebook directory to our import path.
sys.path.append(str(notebook_directory))

# Add the startup directory to our import path.
startup_directory = notebook_directory / "meta" / "startup"
sys.path.append(str(startup_directory))

import argparse
import logging
import os.path
import projectsetup  # noqa: F401
import traceback

from decimal import Decimal
from solana.publickey import PublicKey

from BaseModel import MarginAccount, OpenOrders
from Constants import WARNING_DISCLAIMER_TEXT
from Context import Context, default_cluster, default_cluster_url, default_program_id, default_dex_program_id, default_group_name, default_group_id

# We explicitly want argument parsing to be outside the main try-except block because some arguments
# (like --help) will cause an exit, which our except: block traps.
parser = argparse.ArgumentParser(description="Fetch Mango Markets objects and show their contents.")
parser.add_argument("--cluster", type=str, default=default_cluster,
                    help="Solana RPC cluster name")
parser.add_argument("--cluster-url", type=str, default=default_cluster_url,
                    help="Solana RPC cluster URL")
parser.add_argument("--program-id", type=str, default=default_program_id,
                    help="Mango program ID/address")
parser.add_argument("--dex-program-id", type=str, default=default_dex_program_id,
                    help="DEX program ID/address")
parser.add_argument("--group-name", type=str, default=default_group_name,
                    help="Mango group name")
parser.add_argument("--group-id", type=str, default=default_group_id,
                    help="Mango group ID/address")
parser.add_argument("--id-file", type=str, default="id.json",
                    help="file containing the JSON-formatted wallet private key")
parser.add_argument("--log-level", default=logging.WARNING, type=lambda level: getattr(logging, level),
                    help="level of verbosity to log (possible values: DEBUG, INFO, WARNING, ERROR, CRITICAL)")
parser.add_argument("--type", type=str, required=True,
                    help="type of object to be fetched (can be OPEN-ORDERS or MARGIN-ACCOUNT)")
parser.add_argument("--address", type=PublicKey,
                    help="Solana address of the Mango Markets object to be fetched")
args = parser.parse_args()

logging.getLogger().setLevel(args.log_level)
logging.warning(WARNING_DISCLAIMER_TEXT)

try:
    context = Context(args.cluster, args.cluster_url, args.program_id, args.dex_program_id, args.group_name,
                      args.group_id)

    address = args.address
    object_type = args.type.upper()

    if object_type == "OPEN-ORDERS":
        open_orders = OpenOrders.load(context, address, Decimal(6), Decimal(6))
        print(open_orders)
    elif object_type == "MARGIN-ACCOUNT":
        margin_account = MarginAccount.load(context, address)
        print(margin_account)
    elif object_type == "ALL-MARGIN-ACCOUNTS":
        margin_accounts = MarginAccount.load_all_for_owner(context, address)
        print(margin_accounts)
    else:
        print(f"Unknown type: {object_type}")
except Exception as exception:
    logging.critical(f"mango-fetch stopped because of exception: {exception} - {traceback.format_exc()}")
except:
    logging.critical(f"mango-fetch stopped because of uncatchable error: {traceback.format_exc()}")
