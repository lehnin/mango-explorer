{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ordered-award",
   "metadata": {},
   "source": [
    "# ⚠ Warning\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n",
    "\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/OpinionatedGeek%2Fmango-explorer/HEAD?filepath=Observables.ipynb) _🏃‍♀️ To run this notebook press the ⏩ icon in the toolbar above._\n",
    "\n",
    "[🥭 Mango Markets](https://mango.markets/) support is available at: [Docs](https://docs.mango.markets/) | [Discord](https://discord.gg/67jySBhxrg) | [Twitter](https://twitter.com/mangomarkets) | [Github](https://github.com/blockworks-foundation) | [Email](mailto:hello@blockworks.foundation)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "decreased-fighter",
   "metadata": {},
   "source": [
    "# 🥭 Observables\n",
    "\n",
    "This notebook contains some useful shared tools to work with [RX Observables](https://rxpy.readthedocs.io/en/latest/reference_observable.html).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "chicken-definition",
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "import logging\n",
    "import rx\n",
    "import rx.operators as ops\n",
    "import typing\n",
    "\n",
    "from rxpy_backpressure import BackPressure\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "japanese-skiing",
   "metadata": {},
   "source": [
    "# PrintingObserverSubscriber class\n",
    "\n",
    "This class can subscribe to an `Observable` and print out each item."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "weird-powell",
   "metadata": {},
   "outputs": [],
   "source": [
    "class PrintingObserverSubscriber(rx.core.typing.Observer):\n",
    "    def __init__(self, report_no_output: bool) -> None:\n",
    "        super().__init__()\n",
    "        self.report_no_output = report_no_output\n",
    "\n",
    "    def on_next(self, item: typing.Any) -> None:\n",
    "        self.report_no_output = False\n",
    "        print(item)\n",
    "\n",
    "    def on_error(self, ex: Exception) -> None:\n",
    "        self.report_no_output = False\n",
    "        print(ex)\n",
    "\n",
    "    def on_completed(self) -> None:\n",
    "        if self.report_no_output:\n",
    "            print(\"No items to show.\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "interesting-dream",
   "metadata": {},
   "source": [
    "# TimestampedPrintingObserverSubscriber class\n",
    "\n",
    "Just like `PrintingObserverSubscriber` but it puts a timestamp on each printout."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "broad-apple",
   "metadata": {},
   "outputs": [],
   "source": [
    "class TimestampedPrintingObserverSubscriber(PrintingObserverSubscriber):\n",
    "    def __init__(self, report_no_output: bool) -> None:\n",
    "        super().__init__(report_no_output)\n",
    "\n",
    "    def on_next(self, item: typing.Any) -> None:\n",
    "        super().on_next(f\"{datetime.datetime.now()}: {item}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "received-present",
   "metadata": {},
   "source": [
    "# CollectingObserverSubscriber class\n",
    "\n",
    "This class can subscribe to an `Observable` and collect each item."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "basic-latin",
   "metadata": {},
   "outputs": [],
   "source": [
    "class CollectingObserverSubscriber(rx.core.typing.Observer):\n",
    "    def __init__(self) -> None:\n",
    "        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)\n",
    "        self.collected: typing.List[typing.Any] = []\n",
    "\n",
    "    def on_next(self, item: typing.Any) -> None:\n",
    "        self.collected += [item]\n",
    "\n",
    "    def on_error(self, ex: Exception) -> None:\n",
    "        self.logger.error(f\"Received error: {ex}\")\n",
    "\n",
    "    def on_completed(self) -> None:\n",
    "        pass\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "promotional-blair",
   "metadata": {},
   "source": [
    "# CaptureFirstItem class\n",
    "\n",
    "This captures the first item to pass through the pipeline, allowing it to be instpected later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "figured-dress",
   "metadata": {},
   "outputs": [],
   "source": [
    "class CaptureFirstItem:\n",
    "    def __init__(self):\n",
    "        self.captured: typing.Any = None\n",
    "        self.has_captured: bool = False\n",
    "\n",
    "    def capture_if_first(self, item: typing.Any) -> typing.Any:\n",
    "        if not self.has_captured:\n",
    "            self.captured = item\n",
    "            self.has_captured = True\n",
    "\n",
    "        return item\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "three-reasoning",
   "metadata": {},
   "source": [
    "# FunctionObserver\n",
    "\n",
    "This class takes functions for `on_next()`, `on_error()` and `on_completed()` and returns an `Observer` object.\n",
    "\n",
    "This is mostly for libraries (like `rxpy_backpressure`) that take observers but not their component functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "olive-audit",
   "metadata": {},
   "outputs": [],
   "source": [
    "class FunctionObserver(rx.core.typing.Observer):\n",
    "    def __init__(self,\n",
    "                 on_next: typing.Callable[[typing.Any], None],\n",
    "                 on_error: typing.Callable[[Exception], None] = lambda _: None,\n",
    "                 on_completed: typing.Callable[[], None] = lambda: None):\n",
    "        self._on_next = on_next\n",
    "        self._on_error = on_error\n",
    "        self._on_completed = on_completed\n",
    "\n",
    "    def on_next(self, value: typing.Any) -> None:\n",
    "        self._on_next(value)\n",
    "\n",
    "    def on_error(self, error: Exception) -> None:\n",
    "        self._on_error(error)\n",
    "\n",
    "    def on_completed(self) -> None:\n",
    "        self._on_completed()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "concerned-participation",
   "metadata": {},
   "source": [
    "# create_backpressure_skipping_observer function\n",
    "\n",
    "Creates an `Observer` that skips inputs if they are building up while a subscriber works.\n",
    "\n",
    "This is useful for situations that, say, poll every second but the operation can sometimes take multiple seconds to complete. In that case, the latest item will be immediately emitted and the in-between items skipped."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "promising-substance",
   "metadata": {},
   "outputs": [],
   "source": [
    "def create_backpressure_skipping_observer(on_next: typing.Callable[[typing.Any], None], on_error: typing.Callable[[Exception], None] = lambda _: None, on_completed: typing.Callable[[], None] = lambda: None) -> rx.core.typing.Observer:\n",
    "    observer = FunctionObserver(on_next=on_next, on_error=on_error, on_completed=on_completed)\n",
    "    return BackPressure.LATEST(observer)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "boring-lemon",
   "metadata": {},
   "source": [
    "# debug_print_item function\n",
    "\n",
    "This is a handy item that can be added to a pipeline to show what is being passed at that particular stage. For example, this shows how to print the item before and after filtering:\n",
    "```\n",
    "fetch().pipe(\n",
    "    ops.map(debug_print_item(\"Unfiltered:\")),\n",
    "    ops.filter(lambda item: item.something is not None),\n",
    "    ops.map(debug_print_item(\"Filtered:\")),\n",
    "    ops.filter(lambda item: item.something_else()),\n",
    "    ops.map(act_on_item)\n",
    ").subscribe(some_subscriber)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "saved-reputation",
   "metadata": {},
   "outputs": [],
   "source": [
    "def debug_print_item(title: str) -> typing.Callable[[typing.Any], typing.Any]:\n",
    "    def _debug_print_item(item: typing.Any) -> typing.Any:\n",
    "        print(title, item)\n",
    "        return item\n",
    "    return ops.map(_debug_print_item)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "therapeutic-holder",
   "metadata": {},
   "source": [
    "# log_subscription_error function\n",
    "\n",
    "Logs subscription exceptions to the root logger.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "serious-eugene",
   "metadata": {},
   "outputs": [],
   "source": [
    "def log_subscription_error(error: Exception) -> None:\n",
    "    logging.error(f\"Observable subscription error: {error}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "civil-chester",
   "metadata": {},
   "source": [
    "# observable_pipeline_error_reporter function\n",
    "\n",
    "This intercepts and re-raises an exception, to help report on errors.\n",
    "\n",
    "RxPy pipelines are tricky to restart, so it's often easier to use the `ops.retry()` function in the pipeline. That just swallows the error though, so there's no way to know what was raised to cause the retry.\n",
    "\n",
    "Enter `observable_pipeline_error_reporter()`! Put it in a `catch` just before the `retry` and it should log the error properly.\n",
    "\n",
    "For example:\n",
    "```\n",
    "from rx import of, operators as ops\n",
    "\n",
    "def raise_on_every_third(item):\n",
    "    if (item % 3 == 0):\n",
    "        raise Exception(\"Divisible by 3\")\n",
    "    else:\n",
    "        return item\n",
    "\n",
    "sub1 = of(1, 2, 3, 4, 5, 6).pipe(\n",
    "    ops.map(lambda e : raise_on_every_third(e)),\n",
    "    ops.catch(observable_pipeline_error_reporter),\n",
    "    ops.retry(3)\n",
    ")\n",
    "sub1.subscribe(lambda item: print(item), on_error = lambda error: print(f\"Error : {error}\"))\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "about-scheme",
   "metadata": {},
   "outputs": [],
   "source": [
    "def observable_pipeline_error_reporter(ex, _):\n",
    "    logging.error(f\"Intercepted error in observable pipeline: {ex}\")\n",
    "    raise ex\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "qualified-basis",
   "metadata": {},
   "source": [
    "# Events\n",
    "\n",
    "A strongly(ish)-typed event source that can handle many subscribers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fabulous-quarter",
   "metadata": {},
   "outputs": [],
   "source": [
    "TEventDatum = typing.TypeVar('TEventDatum')\n",
    "\n",
    "\n",
    "class EventSource(rx.subject.Subject, typing.Generic[TEventDatum]):\n",
    "    def __init__(self) -> None:\n",
    "        super().__init__()\n",
    "        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)\n",
    "\n",
    "    def on_next(self, event: TEventDatum) -> None:\n",
    "        super().on_next(event)\n",
    "\n",
    "    def on_error(self, ex: Exception) -> None:\n",
    "        super().on_error(ex)\n",
    "\n",
    "    def on_completed(self) -> None:\n",
    "        super().on_completed()\n",
    "\n",
    "    def publish(self, event: TEventDatum) -> None:\n",
    "        try:\n",
    "            self.on_next(event)\n",
    "        except Exception as exception:\n",
    "            self.logger.warning(f\"Failed to publish event '{event}' - {exception}\")\n",
    "\n",
    "    def dispose(self) -> None:\n",
    "        super().dispose()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "frequent-edition",
   "metadata": {},
   "source": [
    "# 🏃 Running\n",
    "\n",
    "A few quick examples to show how to use these functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "extreme-crime",
   "metadata": {},
   "outputs": [],
   "source": [
    "if __name__ == \"__main__\":\n",
    "    rx.from_([1, 2, 3, 4, 5]).subscribe(PrintingObserverSubscriber(False))\n",
    "    rx.from_([1, 2, 3, 4, 5]).pipe(\n",
    "        ops.filter(lambda item: (item % 2) == 0),\n",
    "    ).subscribe(PrintingObserverSubscriber(False))\n",
    "\n",
    "    collector = CollectingObserverSubscriber()\n",
    "    rx.from_([\"a\", \"b\", \"c\"]).subscribe(collector)\n",
    "    print(collector.collected)\n",
    "\n",
    "    rx.from_([1, 2, 3, 4, 5]).pipe(\n",
    "        ops.map(debug_print_item(\"Before even check:\")),\n",
    "        ops.filter(lambda item: (item % 2) == 0),\n",
    "        ops.map(debug_print_item(\"After even check:\")),\n",
    "    ).subscribe(PrintingObserverSubscriber(True))\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
