{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "enclosed-algebra",
   "metadata": {},
   "source": [
    "# ⚠ Warning\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n",
    "\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/OpinionatedGeek%2Fmango-explorer/HEAD?filepath=Notification.ipynb) _🏃‍♀️ To run this notebook press the ⏩ icon in the toolbar above._\n",
    "\n",
    "[🥭 Mango Markets](https://mango.markets/) support is available at: [Docs](https://docs.mango.markets/) | [Discord](https://discord.gg/67jySBhxrg) | [Twitter](https://twitter.com/mangomarkets) | [Github](https://github.com/blockworks-foundation) | [Email](mailto:hello@blockworks.foundation)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aggressive-physiology",
   "metadata": {},
   "source": [
    "# 🥭 Notification\n",
    "\n",
    "This notebook contains code to send arbitrary notifications.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "patient-blame",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "import abc\n",
    "import csv\n",
    "import logging\n",
    "import os.path\n",
    "import requests\n",
    "import typing\n",
    "\n",
    "from urllib.parse import unquote\n",
    "\n",
    "from BaseModel import LiquidationEvent\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "global-feature",
   "metadata": {},
   "source": [
    "# NotificationTarget class\n",
    "\n",
    "This base class is the root of the different notification mechanisms.\n",
    "\n",
    "Derived classes should override `send_notification()` to implement their own sending logic.\n",
    "\n",
    "Derived classes should not override `send()` since that is the interface outside classes call and it's used to ensure `NotificationTarget`s don't throw an exception when sending."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "noted-marking",
   "metadata": {},
   "outputs": [],
   "source": [
    "class NotificationTarget(metaclass=abc.ABCMeta):\n",
    "    def __init__(self):\n",
    "        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)\n",
    "\n",
    "    def send(self, item: typing.Any) -> None:\n",
    "        try:\n",
    "            self.send_notification(item)\n",
    "        except Exception as exception:\n",
    "            self.logger.error(f\"Error sending {item} - {self} - {exception}\")\n",
    "\n",
    "    @abc.abstractmethod\n",
    "    def send_notification(self, item: typing.Any) -> None:\n",
    "        raise NotImplementedError(\"NotificationTarget.send() is not implemented on the base type.\")\n",
    "\n",
    "    def __repr__(self) -> str:\n",
    "        return f\"{self}\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "forward-compact",
   "metadata": {},
   "source": [
    "# TelegramNotificationTarget class\n",
    "\n",
    "The `TelegramNotificationTarget` sends messages to Telegram.\n",
    "\n",
    "The format for the telegram notification is:\n",
    "1. The word 'telegram'\n",
    "2. A colon ':'\n",
    "3. The chat ID\n",
    "4. An '@' symbol\n",
    "5. The bot token\n",
    "\n",
    "For example:\n",
    "```\n",
    "telegram:<CHAT-ID>@<BOT-TOKEN>\n",
    "```\n",
    "\n",
    "The [Telegram instructions to create a bot](https://core.telegram.org/bots#creating-a-new-bot) show you how to create the bot token."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "quarterly-nothing",
   "metadata": {},
   "outputs": [],
   "source": [
    "class TelegramNotificationTarget(NotificationTarget):\n",
    "    def __init__(self, address):\n",
    "        super().__init__()\n",
    "        chat_id, bot_id = address.split(\"@\", 1)\n",
    "        self.chat_id = chat_id\n",
    "        self.bot_id = bot_id\n",
    "\n",
    "    def send_notification(self, item: typing.Any) -> None:\n",
    "        payload = {\"disable_notification\": True, \"chat_id\": self.chat_id, \"text\": str(item)}\n",
    "        url = f\"https://api.telegram.org/bot{self.bot_id}/sendMessage\"\n",
    "        headers = {\"Content-Type\": \"application/json\"}\n",
    "        requests.post(url, json=payload, headers=headers)\n",
    "\n",
    "    def __str__(self) -> str:\n",
    "        return f\"Telegram chat ID: {self.chat_id}\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "published-ideal",
   "metadata": {},
   "source": [
    "# DiscordNotificationTarget class\n",
    "\n",
    "The `DiscordNotificationTarget` sends messages to Discord.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "junior-conditions",
   "metadata": {},
   "outputs": [],
   "source": [
    "class DiscordNotificationTarget(NotificationTarget):\n",
    "    def __init__(self, address):\n",
    "        super().__init__()\n",
    "        self.address = address\n",
    "\n",
    "    def send_notification(self, item: typing.Any) -> None:\n",
    "        payload = {\n",
    "            \"content\": str(item)\n",
    "        }\n",
    "        url = self.address\n",
    "        headers = {\"Content-Type\": \"application/json\"}\n",
    "        requests.post(url, json=payload, headers=headers)\n",
    "\n",
    "    def __str__(self) -> str:\n",
    "        return \"Discord webhook\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "actual-bronze",
   "metadata": {},
   "source": [
    "# MailjetNotificationTarget class\n",
    "\n",
    "The `MailjetNotificationTarget` sends an email through [Mailjet](https://mailjet.com).\n",
    "\n",
    "In order to pass everything in to the notifier as a single string (needed to stop command-line parameters form getting messy), `MailjetNotificationTarget` requires a compound string, separated by colons.\n",
    "```\n",
    "mailjet:<MAILJET-API-KEY>:<MAILJET-API-SECRET>:FROM-NAME:FROM-ADDRESS:TO-NAME:TO-ADDRESS\n",
    "\n",
    "```\n",
    "Individual components are URL-encoded (so, for example, spaces are replaces with %20, colons are replaced with %3A).\n",
    "\n",
    "* `<MAILJET-API-KEY>` and `<MAILJET-API-SECRET>` are from your [Mailjet](https://mailjet.com) account.\n",
    "* `FROM-NAME` and `TO-NAME` are just text fields that are used as descriptors in the email messages.\n",
    "* `FROM-ADDRESS` is the address the email appears to come from. This must be validated with [Mailjet](https://mailjet.com).\n",
    "* `TO-ADDRESS` is the destination address - the email account to which the email is being sent.\n",
    "\n",
    "Mailjet provides a client library, but really we don't need or want more dependencies. This code just replicates the `curl` way of doing things:\n",
    "```\n",
    "curl -s \\\n",
    "\t-X POST \\\n",
    "\t--user \"$MJ_APIKEY_PUBLIC:$MJ_APIKEY_PRIVATE\" \\\n",
    "\thttps://api.mailjet.com/v3.1/send \\\n",
    "\t-H 'Content-Type: application/json' \\\n",
    "\t-d '{\n",
    "      \"SandboxMode\":\"true\",\n",
    "      \"Messages\":[\n",
    "        {\n",
    "          \"From\":[\n",
    "            {\n",
    "              \"Email\":\"pilot@mailjet.com\",\n",
    "              \"Name\":\"Your Mailjet Pilot\"\n",
    "            }\n",
    "          ],\n",
    "          \"HTMLPart\":\"<h3>Dear passenger, welcome to Mailjet!</h3><br />May the delivery force be with you!\",\n",
    "          \"Subject\":\"Your email flight plan!\",\n",
    "          \"TextPart\":\"Dear passenger, welcome to Mailjet! May the delivery force be with you!\",\n",
    "          \"To\":[\n",
    "            {\n",
    "              \"Email\":\"passenger@mailjet.com\",\n",
    "              \"Name\":\"Passenger 1\"\n",
    "            }\n",
    "          ]\n",
    "        }\n",
    "      ]\n",
    "\t}'\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "reduced-rabbit",
   "metadata": {},
   "outputs": [],
   "source": [
    "class MailjetNotificationTarget(NotificationTarget):\n",
    "    def __init__(self, encoded_parameters):\n",
    "        super().__init__()\n",
    "        self.address = \"https://api.mailjet.com/v3.1/send\"\n",
    "        api_key, api_secret, subject, from_name, from_address, to_name, to_address = encoded_parameters.split(\":\")\n",
    "        self.api_key: str = unquote(api_key)\n",
    "        self.api_secret: str = unquote(api_secret)\n",
    "        self.subject: str = unquote(subject)\n",
    "        self.from_name: str = unquote(from_name)\n",
    "        self.from_address: str = unquote(from_address)\n",
    "        self.to_name: str = unquote(to_name)\n",
    "        self.to_address: str = unquote(to_address)\n",
    "\n",
    "    def send_notification(self, item: typing.Any) -> None:\n",
    "        payload = {\n",
    "            \"Messages\": [\n",
    "                {\n",
    "                    \"From\": {\n",
    "                        \"Email\": self.from_address,\n",
    "                        \"Name\": self.from_name\n",
    "                    },\n",
    "                    \"Subject\": self.subject,\n",
    "                    \"TextPart\": str(item),\n",
    "                    \"To\": [\n",
    "                        {\n",
    "                            \"Email\": self.to_address,\n",
    "                            \"Name\": self.to_name\n",
    "                        }\n",
    "                    ]\n",
    "                }\n",
    "            ]\n",
    "        }\n",
    "\n",
    "        url = self.address\n",
    "        headers = {\"Content-Type\": \"application/json\"}\n",
    "        requests.post(url, json=payload, headers=headers, auth=(self.api_key, self.api_secret))\n",
    "\n",
    "    def __str__(self) -> str:\n",
    "        return f\"Mailjet notifications to '{self.to_name}' '{self.to_address}' with subject '{self.subject}'\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "gross-stereo",
   "metadata": {},
   "source": [
    "# CsvFileNotificationTarget class\n",
    "\n",
    "Outputs a liquidation event to CSV. Nothing is written if the item is not a `LiquidationEvent`.\n",
    "\n",
    "Headers for the CSV file should be:\n",
    "```\n",
    "\"Timestamp\",\"Liquidator Name\",\"Group\",\"Succeeded\",\"Signature\",\"Wallet\",\"Margin Account\",\"Token Changes\"\n",
    "```\n",
    "Token changes are listed as pairs of value plus symbol, so each token change adds two columns to the output. Token changes may arrive in different orders, so ordering of token changes is not guaranteed to be consistent from transaction to transaction.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "given-african",
   "metadata": {},
   "outputs": [],
   "source": [
    "class CsvFileNotificationTarget(NotificationTarget):\n",
    "    def __init__(self, filename):\n",
    "        super().__init__()\n",
    "        self.filename = filename\n",
    "\n",
    "    def send_notification(self, item: typing.Any) -> None:\n",
    "        if isinstance(item, LiquidationEvent):\n",
    "            event: LiquidationEvent = item\n",
    "            if not os.path.isfile(self.filename) or os.path.getsize(self.filename) == 0:\n",
    "                with open(self.filename, \"w\") as empty_file:\n",
    "                    empty_file.write('\"Timestamp\",\"Liquidator Name\",\"Group\",\"Succeeded\",\"Signature\",\"Wallet\",\"Margin Account\",\"Token Changes\"\\n')\n",
    "\n",
    "            with open(self.filename, \"a\") as csvfile:\n",
    "                result = \"Succeeded\" if event.succeeded else \"Failed\"\n",
    "                row_data = [event.timestamp, event.liquidator_name, event.group_name, result, event.signature, event.wallet_address, event.margin_account_address]\n",
    "                for change in event.changes:\n",
    "                    row_data += [f\"{change.value:.8f}\", change.token.name]\n",
    "                file_writer = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)\n",
    "                file_writer.writerow(row_data)\n",
    "\n",
    "    def __str__(self) -> str:\n",
    "        return f\"CSV notifications to file {self.filename}\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adult-chicago",
   "metadata": {},
   "source": [
    "# FilteringNotificationTarget class\n",
    "\n",
    "This class takes a `NotificationTarget` and a filter function, and only calls the `NotificationTarget` if the filter function returns `True` for the notification item."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acceptable-christopher",
   "metadata": {},
   "outputs": [],
   "source": [
    "class FilteringNotificationTarget(NotificationTarget):\n",
    "    def __init__(self, inner_notifier: NotificationTarget, filter_func: typing.Callable[[typing.Any], bool]):\n",
    "        super().__init__()\n",
    "        self.filter_func = filter_func\n",
    "        self.inner_notifier: NotificationTarget = inner_notifier\n",
    "\n",
    "    def send_notification(self, item: typing.Any) -> None:\n",
    "        if self.filter_func(item):\n",
    "            self.inner_notifier.send_notification(item)\n",
    "\n",
    "    def __str__(self) -> str:\n",
    "        return f\"Filtering notification target for '{self.inner_notifier}'\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "falling-cattle",
   "metadata": {},
   "source": [
    "# parse_subscription_target() function\n",
    "\n",
    "`parse_subscription_target()` takes a parameter as a string and returns a notification target.\n",
    "\n",
    "This is most likely used when parsing command-line arguments - this function can be used in the `type` parameter of an `add_argument()` call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "speaking-vocabulary",
   "metadata": {},
   "outputs": [],
   "source": [
    "def parse_subscription_target(target):\n",
    "    protocol, destination = target.split(\":\", 1)\n",
    "\n",
    "    if protocol == \"telegram\":\n",
    "        return TelegramNotificationTarget(destination)\n",
    "    elif protocol == \"discord\":\n",
    "        return DiscordNotificationTarget(destination)\n",
    "    elif protocol == \"mailjet\":\n",
    "        return MailjetNotificationTarget(destination)\n",
    "    elif protocol == \"csvfile\":\n",
    "        return CsvFileNotificationTarget(destination)\n",
    "    else:\n",
    "        raise Exception(f\"Unknown protocol: {protocol}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bacterial-coffee",
   "metadata": {},
   "source": [
    "## NotificationHandler class\n",
    "\n",
    "A bridge between the worlds of notifications and logging. This allows any `NotificationTarget` to be plugged in to the `logging` subsystem to receive log messages and notify however it chooses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "norman-bruce",
   "metadata": {},
   "outputs": [],
   "source": [
    "class NotificationHandler(logging.StreamHandler):\n",
    "    def __init__(self, target: NotificationTarget):\n",
    "        logging.StreamHandler.__init__(self)\n",
    "        self.target = target\n",
    "\n",
    "    def emit(self, record):\n",
    "        message = self.format(record)\n",
    "        self.target.send_notification(message)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cloudy-czech",
   "metadata": {},
   "source": [
    "# ✅ Testing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "rough-structure",
   "metadata": {},
   "outputs": [],
   "source": [
    "def _notebook_tests():\n",
    "    test_target = parse_subscription_target(\"telegram:chat@bot\")\n",
    "\n",
    "    assert(test_target.chat_id == \"chat\")\n",
    "    assert(test_target.bot_id == \"bot\")\n",
    "\n",
    "    mailjet_target_string = \"user:secret:subject:from%20name:from@address:to%20name%20with%20colon%3A:to@address\"\n",
    "    mailjet_target = MailjetNotificationTarget(mailjet_target_string)\n",
    "    assert(mailjet_target.api_key == \"user\")\n",
    "    assert(mailjet_target.api_secret == \"secret\")\n",
    "    assert(mailjet_target.subject == \"subject\")\n",
    "    assert(mailjet_target.from_name == \"from name\")\n",
    "    assert(mailjet_target.from_address == \"from@address\")\n",
    "    assert(mailjet_target.to_name == \"to name with colon:\")\n",
    "    assert(mailjet_target.to_address == \"to@address\")\n",
    "\n",
    "    parse_subscription_target(\"telegram:012345678@9876543210:ABCDEFGHijklmnop-qrstuvwxyzABCDEFGH\")\n",
    "    parse_subscription_target(\"discord:https://discord.com/api/webhooks/012345678901234567/ABCDE_fghij-KLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN\")\n",
    "    parse_subscription_target(\"mailjet:user:secret:subject:from%20name:from@address:to%20name%20with%20colon%3A:to@address\")\n",
    "    parse_subscription_target(\"csvfile:filename.csv\")\n",
    "\n",
    "    class MockNotificationTarget(NotificationTarget):\n",
    "        def __init__(self):\n",
    "            super().__init__()\n",
    "            self.send_notification_called = False\n",
    "\n",
    "        def send_notification(self, item: typing.Any) -> None:\n",
    "            self.send_notification_called = True\n",
    "\n",
    "    mock = MockNotificationTarget()\n",
    "    filtering = FilteringNotificationTarget(mock, lambda x: x == \"yes\")\n",
    "    filtering.send(\"no\")\n",
    "    assert(not mock.send_notification_called)\n",
    "    filtering.send(\"yes\")\n",
    "    assert(mock.send_notification_called)\n",
    "\n",
    "\n",
    "_notebook_tests()\n",
    "del _notebook_tests"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "solved-switzerland",
   "metadata": {},
   "source": [
    "# 🏃 Running\n",
    "\n",
    "A few quick examples to show how to use these functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "signed-guyana",
   "metadata": {},
   "outputs": [],
   "source": [
    "if __name__ == \"__main__\":\n",
    "    def _notebook_run():\n",
    "        log_level = logging.getLogger().level\n",
    "        try:\n",
    "            test_telegram_target = parse_subscription_target(\"telegram:012345678@9876543210:ABCDEFGHijklmnop-qrstuvwxyzABCDEFGH\")\n",
    "            print(test_telegram_target)\n",
    "\n",
    "            test_discord_target = parse_subscription_target(\"discord:https://discord.com/api/webhooks/012345678901234567/ABCDE_fghij-KLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN\")\n",
    "            print(test_discord_target)\n",
    "\n",
    "            test_mailjet_target = parse_subscription_target(\"mailjet:user:secret:Subject%20text:from%20name:from@address:to%20name%20with%20colon%3A:to@address\")\n",
    "            print(test_mailjet_target)\n",
    "\n",
    "            test_csv_target = parse_subscription_target(\"csvfile:liquidations.csv\")\n",
    "            print(test_csv_target)\n",
    "\n",
    "            #  These lines, if uncommented, will create and write to the file.\n",
    "            #  import datetime\n",
    "            #  from BaseModel import TokenLookup, TokenValue\n",
    "            #  from Context import default_context\n",
    "            #  from decimal import Decimal\n",
    "            #  from Wallet import default_wallet\n",
    "            #  balances_before = [\n",
    "            #      TokenValue(TokenLookup.find_by_name(default_context, \"ETH\"), Decimal(1)),\n",
    "            #      TokenValue(TokenLookup.find_by_name(default_context, \"BTC\"), Decimal(\"0.1\")),\n",
    "            #      TokenValue(TokenLookup.find_by_name(default_context, \"USDT\"), Decimal(1000))\n",
    "            #  ]\n",
    "            #  balances_after = [\n",
    "            #      TokenValue(TokenLookup.find_by_name(default_context, \"ETH\"), Decimal(1)),\n",
    "            #      TokenValue(TokenLookup.find_by_name(default_context, \"BTC\"), Decimal(\"0.05\")),\n",
    "            #      TokenValue(TokenLookup.find_by_name(default_context, \"USDT\"), Decimal(2000))\n",
    "            #  ]\n",
    "            #\n",
    "            #  event = LiquidationEvent(datetime.datetime.now(),\n",
    "            #                           \"SIGNATURE\",\n",
    "            #                           default_wallet.address,\n",
    "            #                           default_wallet.address,\n",
    "            #                           balances_before,\n",
    "            #                           balances_after)\n",
    "            #  test_csv_target.send(event)\n",
    "        finally:\n",
    "            logging.getLogger().setLevel(log_level)\n",
    "\n",
    "    _notebook_run()\n",
    "    del _notebook_run\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
